package newtours.pages;

import newtours.pages.login.LoggedUserPage;
import newtours.pages.login.LoginPage;
import newtours.pages.register.RegisterPage;
import newtours.pages.register.RegisteredPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageManager {

    private final WebDriver webDriver;
    private LoginPage loginPage;
    private LoggedUserPage loggedUserPage;
    private RegisterPage registerPage;
    private RegisteredPage registeredPage;

    public PageManager(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public LoginPage loginPage() {
        if (loginPage == null) {
            loginPage = PageFactory.initElements(webDriver, LoginPage.class);
        }
        return loginPage;
    }

    public LoggedUserPage loggedUserPage() {
        if (loggedUserPage == null) {
            loggedUserPage = PageFactory.initElements(webDriver, LoggedUserPage.class);
        }
        return loggedUserPage;
    }

    public RegisterPage registerPage() {
        if (registerPage == null) {
            registerPage = PageFactory.initElements(webDriver, RegisterPage.class);
        }
        return registerPage;
    }

    public RegisteredPage registeredPage() {
        if (registeredPage == null) {
            registeredPage = PageFactory.initElements(webDriver, RegisteredPage.class);
        }
        return registeredPage;
    }
}
