package newtours.pages.register;

import common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisteredPage extends BasePage<RegisteredPage> {

    @FindBy(css = "tr:nth-child(3) > td > p:nth-child(1) > font > b")
    private WebElement pageIdentification;

    public RegisteredPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    public boolean isLoaded() {
        return custom.isElementVisible(pageIdentification);
    }
}
