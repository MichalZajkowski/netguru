package common;

import framework.utils.Configuration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.InvalidParameterException;
import java.util.Objects;

public class BaseTest {

    private static Configuration configuration = Configuration.getInstance();
    private static final String DRIVER = "driver";
    private static Logger logger = LoggerFactory.getLogger(BaseTest.class);
    protected static WebDriver webDriver;

    protected BaseTest() {
        setDriver();
    }

    private void setDriver() throws InvalidParameterException {
        try {
            logger.debug("Setup geckoDriver");
            Configuration.setProperty(DRIVER, "firefox");
            Configuration.setProperty("webdriver.gecko.driver", configuration.getPropertyFromFile("geckoDriver"));
            webDriver = new FirefoxDriver();
        } finally {
            logger.debug("Maximize window");
            Objects.requireNonNull(webDriver).manage().window().maximize();
        }
    }
}