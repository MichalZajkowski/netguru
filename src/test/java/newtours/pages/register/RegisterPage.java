package newtours.pages.register;

import common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RegisterPage extends BasePage<RegisterPage> {

    @FindBy(name = "email")
    private WebElement userNameInput;

    @FindBy(name = "password")
    private List<WebElement> userPasswordInput;

    @FindBy(name = "register")
    private WebElement registerButton;

    public RegisterPage(WebDriver webDriver) {
        super(webDriver);
    }

    public RegisterPage fillUserNameInput(String userName) {
        custom.clearAndSendKey(userNameInput, userName);
        return this;
    }

    public RegisterPage fillPasswordInput(String password) {
        for (WebElement i : userPasswordInput) {
            custom.clearAndSendKey(i, password);
        }
        return this;
    }

    public RegisterPage clickRegisterButton() {
        custom.clickElement(registerButton, 1);
        return this;
    }

    @Override
    public boolean isLoaded() {
        return custom.isElementVisible(userNameInput);
    }
}
