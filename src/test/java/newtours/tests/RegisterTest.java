package newtours.tests;

import common.BaseTest;
import newtours.pages.PageManager;
import org.junit.AfterClass;
import org.junit.Test;

public class RegisterTest extends BaseTest {

    private static String url = "http://newtours.demoaut.com/mercuryregister.php";
    private static String userName = "test@email.com";
    private static String password = "test";
    private PageManager manager;

    public RegisterTest() {
        manager = new PageManager(webDriver);
    }

    @AfterClass
    public static void treadDown() {
        webDriver.quit();
    }

    @Test
    public void loginTest() {
        manager.registerPage()
                .loadPage(url);
        manager.registerPage()
                .isLoaded();
        manager.registerPage()
                .fillPasswordInput(password)
                .fillUserNameInput(userName)
                .clickRegisterButton();
        manager.registeredPage()
                .isLoaded();
    }
}
