package common;

import framework.utils.Custom;
import org.openqa.selenium.WebDriver;

public abstract class BasePage<T> {

    public WebDriver webDriver;
    protected Custom custom;

    public BasePage(WebDriver webDriver) {
        this.webDriver = webDriver;
        custom = new Custom(webDriver);
    }

    public abstract boolean isLoaded();

    public BasePage loadPage(String url) {
        webDriver.get(url);
        return this;
    }
}