package newtours.tests;

import common.BaseTest;
import newtours.pages.PageManager;
import org.junit.AfterClass;
import org.junit.Test;
public class LoginTest extends BaseTest {

    private static String url = "http://newtours.demoaut.com/mercurysignon.php";
    private static String userName = "test@email.com";
    private static String password = "test";
    private PageManager manager;

    public LoginTest() {
        manager = new PageManager(webDriver);
    }

    @AfterClass
    public static void treadDown() {
        webDriver.quit();
    }

    @Test
    public void loginTest() {
        manager.loginPage()
                .loadPage(url);
        manager.loginPage()
                .isLoaded();
        manager.loginPage()
                .fillPasswordInput(password)
                .fillUserNameInput(userName)
                .clickLoginButton();
        manager.loggedUserPage()
                .isLoaded();
    }
}
