package newtours.pages.login;

import common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoggedUserPage extends BasePage<LoggedUserPage> {

    @FindBy(name = "findFlights")
    private WebElement pageIdentification;

    public LoggedUserPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    public boolean isLoaded() {
        return custom.isElementVisible(pageIdentification);
    }
}
