package newtours.pages.login;

import common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage  extends BasePage<LoginPage> {

    @FindBy(name = "userName")
    private WebElement userNameInput;

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(name = "login")
    private WebElement loginButton;

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    public LoginPage fillUserNameInput(String userName){
        custom.clearAndSendKey(userNameInput, userName);
        return this;
    }

    public LoginPage fillPasswordInput(String password){
        custom.clearAndSendKey(passwordInput, password);
        return this;
    }

    public LoginPage clickLoginButton(){
        custom.clickElement(loginButton,1);
        return this;
    }

    @Override
    public boolean isLoaded() {
        return custom.isElementVisible(passwordInput);
    }
}
