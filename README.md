# Netguru

Sample repository created for recruitment purposes. Project based on Maven and Java.

To run the login test, use the command:
 - mvn clean test -Dtest=LoginTest

To run the register test, use the command:
 - mvn clean test -Dtest=RegisterTest
